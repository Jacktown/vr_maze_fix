﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Screenshots : MonoBehaviour {

    public GameObject camMap;
    public Image imageUI;
	private Camera camTr;

    private void Start()
    {
        
    }

	public void TakePicture(int w, int h)
    {
        camMap.SetActive(true);

		// adapt the zoom according to maze dimensions
		camTr = camMap.GetComponent<Camera> ();
		int sizeScale = (w > h) ? w : h;
		camTr.orthographicSize = 0.7f * sizeScale - 1.0f;

        StartCoroutine(ExecuteAfterTime(2f));
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        StartCoroutine(ReadScreen());
    }

    IEnumerator ReadScreen()
    {
        yield return new WaitForEndOfFrame();

		Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        //tex.ReadPixels(new Rect(10, 10, 520, 490), 5, 5, false);
		tex.ReadPixels (new Rect(0,0, Screen.width, Screen.height), 5, 5);
        tex.Apply();
		//Debug.Log ("Picture Taken");
        Sprite levelMap = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0), 100.0f);
        imageUI.sprite = levelMap;
        camMap.SetActive(false);
    }

    /*
    public Sprite LoadNewSprite(string FilePath, float PixelsPerUnit = 100.0f)
    {
        // Load a PNG or JPG image from disk to a Texture2D, assign this texture to a new sprite and return its reference

        Sprite NewSprite = new Sprite();
        Texture2D SpriteTexture = LoadTexture(FilePath);
        NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), new Vector2(0, 0), PixelsPerUnit);

        return NewSprite;
    }

    public Texture2D LoadTexture(string FilePath)
    {
        // Load a PNG or JPG file from disk to a Texture2D
        // Returns null if load fails

        Texture2D Tex2D;
        byte[] FileData;

        if (File.Exists(FilePath))
        {
            FileData = File.ReadAllBytes(FilePath);
            Tex2D = new Texture2D(2, 2);           // Create new "empty" texture
            if (Tex2D.LoadImage(FileData))           // Load the imagedata into the texture (size is set automatically)
                return Tex2D;                 // If data = readable -> return texture
        }
        return null;                     // Return null if load failed
    }*/
}

