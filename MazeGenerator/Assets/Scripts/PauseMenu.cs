﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class PauseMenu : MonoBehaviour {

    public GameObject rightController;
    public GameObject leftController;

    public GameObject mainMenu;
    public GameObject transitionMenu;

    private bool isPause = false;
    private void Start()
    {
        transform.GetChild(0).gameObject.SetActive(isPause);
    }
    public void TogglePause()
    {
        if (mainMenu.activeSelf == false && transitionMenu.activeSelf == false)
        {
            isPause = !isPause;
            transform.GetChild(0).gameObject.SetActive(isPause);
            rightController.GetComponent<VRTK_Pointer>().enabled = isPause;
            leftController.GetComponent<VRTK_Pointer>().enabled = !isPause;
        }
        else
        {
            isPause = false;
            transform.GetChild(0).gameObject.SetActive(isPause);
            rightController.GetComponent<VRTK_Pointer>().enabled = !isPause;
            leftController.GetComponent<VRTK_Pointer>().enabled = isPause;
        }
    }
}
