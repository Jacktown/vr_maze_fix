﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BackUp : MonoBehaviour
{

    [Header("Standard Settings")]
    [Range(10, 50)]
    public int width = 20;
    [Range(10, 50)]
    public int height = 20;
    [Range(1, 6)]
    public int nbExit = 2;

    [Header("Advanced Settings")]
    [Range(20, 75)]
    public int horizontalPercentage = 45;
    [Range(20, 95)]
    public int verticalPercentage = 90;
    [Range(40, 95)]
    public int roomPercentage = 75;
    [Range(1, 100)]
    public int wallDiversity = 20;

    [Header("Assets")]
    public GameObject wall;
    public GameObject block;
    public GameObject door;
    public GameObject rig;

    int[,] wallHorizontal;
    int[,] wallVertical;
    int[,] drawMap;
    float scale = 5;
    int score = 0;

    List<GameObject> instantiated = new List<GameObject>();

    WallGeneration wallGenerator;

    struct Position
    {
        public int posX;
        public int posY;

        public Position(int x, int y)
        {
            posX = x;
            posY = y;
        }
    }

    void Start()
    {
        wallGenerator = gameObject.GetComponent<WallGeneration>();
        //GenerateMap();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GenerateMap();
        }
    }

    public void GenerateMap()
    {
        if (instantiated.Count != 0)
        {
            score++;
            foreach (GameObject toDestroy in instantiated)
                Destroy(toDestroy);
        }
        int nbTry = 0;
        List<List<Position>> regions;
        do
        {
            wallHorizontal = new int[width, height + 1];
            wallVertical = new int[width + 1, height];
            drawMap = new int[width, height];
            FillMap();
            FixMap();
            regions = GetRegions();
            regions.Sort((a, b) => a.Count - b.Count);
            nbTry++;
        } while (regions[regions.Count - 1].Count < (width * height * (roomPercentage / 100.0f)) && nbTry < 5000);
        Position entryPos = DefineEntry(regions[regions.Count - 1]);
        DefineExit(regions[regions.Count - 1], entryPos);
        DrawBlocks(regions);
        //Debug.Log("Number of regions: " + regions.Count);
        //Debug.Log("Size of biggest region: " + regions[regions.Count - 1].Count);
        //Debug.Log("Size of smallest region: " + regions[0].Count);
        //Debug.Log("Room treshold: " + width * height * (roomPercentage / 100.0f));
        //Debug.Log("Number of tries: " + nbTry);
        Debug.Log("Score: " + score);
        Make3D();
    }

    void FillMap()
    {
        for (int y = 0; y < wallHorizontal.GetLength(1); y++)
        {
            for (int x = 0; x < wallHorizontal.GetLength(0); x++)
            {
                if (y == 0 || y == wallHorizontal.GetLength(1) - 1)
                {
                    wallHorizontal[x, y] = 1;
                }
                else
                {
                    wallHorizontal[x, y] = (UnityEngine.Random.Range(0, 100) < horizontalPercentage) ? 1 : 0;
                }
            }
        }

        for (int y = 0; y < wallVertical.GetLength(1); y++)
        {
            for (int x = 0; x < wallVertical.GetLength(0); x++)
            {
                if (x == 0 || x == wallVertical.GetLength(0) - 1)
                {
                    wallVertical[x, y] = 1;
                }
                else
                {
                    switch (wallHorizontal[x - 1, y] + wallHorizontal[x - 1, y + 1] + wallHorizontal[x, y] + wallHorizontal[x, y + 1])
                    {
                        case 0:
                            wallVertical[x, y] = 1;
                            break;
                        case 1:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage) ? 1 : 0;
                            break;
                        case 2:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 2) ? 1 : 0;
                            break;
                        case 3:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 4) ? 1 : 0;
                            break;
                        case 4:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 8) ? 1 : 0;
                            break;
                        default:
                            wallVertical[x, y] = wallVertical[x, y];
                            break;
                    }
                }
            }
        }
    }

    List<List<Position>> GetRegions()
    {
        List<List<Position>> regions = new List<List<Position>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0)
                {
                    List<Position> newRegion = GetRegion(x, y);
                    regions.Add(newRegion);

                    foreach (Position tile in newRegion)
                    {
                        mapFlags[tile.posX, tile.posY] = 1;
                    }
                }
            }
        }
        return regions;
    }

    List<Position> GetRegion(int startX, int startY)
    {
        List<Position> region = new List<Position>();
        int[,] mapFlags = new int[width, height];

        Queue<Position> toVisit = new Queue<Position>();
        toVisit.Enqueue(new Position(startX, startY));
        mapFlags[startX, startY] = 1;

        while (toVisit.Count > 0)
        {
            Position tile = toVisit.Dequeue();
            region.Add(tile);

            if (IsInMapRange(tile.posX, tile.posY - 1) && mapFlags[tile.posX, tile.posY - 1] == 0 && wallHorizontal[tile.posX, tile.posY] == 0)
            {
                mapFlags[tile.posX, tile.posY - 1] = 1;
                toVisit.Enqueue(new Position(tile.posX, tile.posY - 1));
            }
            if (IsInMapRange(tile.posX - 1, tile.posY) && mapFlags[tile.posX - 1, tile.posY] == 0 && wallVertical[tile.posX, tile.posY] == 0)
            {
                mapFlags[tile.posX - 1, tile.posY] = 1;
                toVisit.Enqueue(new Position(tile.posX - 1, tile.posY));
            }
            if (IsInMapRange(tile.posX, tile.posY + 1) && mapFlags[tile.posX, tile.posY + 1] == 0 && wallHorizontal[tile.posX, tile.posY + 1] == 0)
            {
                mapFlags[tile.posX, tile.posY + 1] = 1;
                toVisit.Enqueue(new Position(tile.posX, tile.posY + 1));
            }
            if (IsInMapRange(tile.posX + 1, tile.posY) && mapFlags[tile.posX + 1, tile.posY] == 0 && wallVertical[tile.posX + 1, tile.posY] == 0)
            {
                mapFlags[tile.posX + 1, tile.posY] = 1;
                toVisit.Enqueue(new Position(tile.posX + 1, tile.posY));
            }
        }
        return region;
    }

    void FixMap()
    {
        int isolatedCountH = 0;
        int isolatedCountV = 0;
        for (int y = 0; y < wallHorizontal.GetLength(1); y++)
        {
            for (int x = 0; x < wallHorizontal.GetLength(0); x++)
            {
                if (IsIsolated(x, y, false) && wallHorizontal[x, y] == 1)
                {
                    isolatedCountH++;
                    LinkToNeighbour(x, y, false);
                }
            }
        }

        for (int y = 0; y < wallVertical.GetLength(1); y++)
        {
            for (int x = 0; x < wallVertical.GetLength(0); x++)
            {
                if (IsIsolated(x, y, true) && wallVertical[x, y] == 1)
                {
                    isolatedCountV++;
                    LinkToNeighbour(x, y, true);
                }
            }
        }
        //Debug.Log("Isolated Horizontal walls: " + isolatedCountH + " ; Isolated Vertical walls: " + isolatedCountV);
        //Debug.Log("Horizontal walls: " + CountHorizontalWalls() + " ; Vertical walls: " + CountVerticalWalls());
    }

    int CountHorizontalWalls()
    {
        int horiCount = 0;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height + 1; y++)
            {
                if (wallHorizontal[x, y] != 0)
                {
                    horiCount++;
                }
            }
        }
        return horiCount;
    }

    int CountVerticalWalls()
    {
        int vertCount = 0;
        for (int x = 0; x < width + 1; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (wallVertical[x, y] != 0)
                {
                    vertCount++;
                }
            }
        }
        return vertCount;
    }

    bool IsIsolated(int x, int y, bool vertical)
    {
        bool ret = false;
        if (vertical)
        {
            if (x != 0 && x != wallVertical.GetLength(0) - 1 && y != 0 && y != wallVertical.GetLength(1) - 1)
            {
                if (wallHorizontal[x, y] == 0 && wallHorizontal[x, y + 1] == 0 && wallHorizontal[x - 1, y] == 0 && wallHorizontal[x - 1, y + 1] == 0 && wallVertical[x, y - 1] == 0 && wallVertical[x, y + 1] == 0)
                    ret = true;
            }
        }
        else
        {
            if (y != 0 && y != wallHorizontal.GetLength(1) - 1 && x != 0 && x != wallHorizontal.GetLength(0) - 1)
            {
                if (wallVertical[x, y] == 0 && wallVertical[x, y - 1] == 0 && wallVertical[x + 1, y] == 0 && wallVertical[x + 1, y - 1] == 0 && wallHorizontal[x - 1, y] == 0 && wallHorizontal[x + 1, y] == 0)
                    ret = true;
            }
        }
        return ret;
    }

    void LinkToNeighbour(int x, int y, bool vertical)
    {
        if (CountHorizontalWalls() > CountVerticalWalls())    //add vertical walls
        {
            if (vertical)
            {
                if (UnityEngine.Random.Range(0, 100) < 50)
                {
                    wallVertical[x, y + 1] = 2;
                }
                else
                {
                    wallVertical[x, y - 1] = 2;
                }
            }
            else
            {
                bool solved = false;
                while (!solved)
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                            if (wallHorizontal[x - 1, y - 1] == 1 || wallHorizontal[x, y - 1] == 1 || IsReachableVertical(x, y - 2))
                            {
                                solved = true;
                                wallVertical[x, y - 1] = 2;
                            }
                            break;
                        case 1:
                            if (wallHorizontal[x - 1, y + 1] == 1 || wallHorizontal[x, y + 1] == 1 || IsReachableVertical(x, y + 1))
                            {
                                solved = true;
                                wallVertical[x, y] = 2;
                            }
                            break;
                        case 2:
                            if (wallHorizontal[x + 1, y + 1] == 1 || wallHorizontal[x, y + 1] == 1 || IsReachableVertical(x + 1, y + 1))
                            {
                                solved = true;
                                wallVertical[x + 1, y] = 2;
                            }
                            break;
                        case 3:
                            if (wallHorizontal[x + 1, y - 1] == 1 || wallHorizontal[x, y - 1] == 1 || IsReachableVertical(x + 1, y - 2))
                            {
                                solved = true;
                                wallVertical[x + 1, y - 1] = 2;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        else   //add horizontal walls
        {
            if (vertical)
            {
                bool solved = false;
                while (!solved)
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                            if (wallVertical[x - 1, y - 1] == 1 || wallVertical[x - 1, y] == 1 || IsReachableHorizontal(x - 2, y))
                            {
                                solved = true;
                                wallHorizontal[x - 1, y] = 2;
                            }
                            break;
                        case 1:
                            if (wallVertical[x - 1, y + 1] == 1 || wallVertical[x - 1, y] == 1 || IsReachableHorizontal(x - 2, y + 1))
                            {
                                solved = true;
                                wallHorizontal[x - 1, y + 1] = 2;
                            }
                            break;
                        case 2:
                            if (wallVertical[x + 1, y + 1] == 1 || wallVertical[x + 1, y] == 1 || IsReachableHorizontal(x + 1, y + 1))
                            {
                                solved = true;
                                wallHorizontal[x, y + 1] = 2;
                            }
                            break;
                        case 3:
                            if (wallVertical[x + 1, y - 1] == 1 || wallVertical[x + 1, y] == 1 || IsReachableHorizontal(x + 1, y))
                            {
                                solved = true;
                                wallHorizontal[x, y] = 2;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                if (UnityEngine.Random.Range(0, 100) < 50)
                {
                    wallHorizontal[x + 1, y] = 2;
                }
                else
                {
                    wallHorizontal[x - 1, y] = 2;
                }
            }
        }
    }

    bool IsReachableVertical(int x, int y)
    {
        bool ret = false;
        if (y < wallVertical.GetLength(1) && y >= 0)
        {
            if (wallVertical[x, y] == 1)
                ret = true;
        }
        return ret;
    }

    bool IsReachableHorizontal(int x, int y)
    {
        bool ret = false;
        if (x < wallHorizontal.GetLength(0) && x >= 0)
        {
            if (wallHorizontal[x, y] == 1)
                ret = true;
        }
        return ret;
    }

    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    void DrawBlocks(List<List<Position>> _regions)
    {
        for (int i = 0; i < _regions.Count - 1; i++)
        {
            List<Position> newRegion = _regions[i];
            foreach (Position tile in newRegion)
            {
                drawMap[tile.posX, tile.posY] = 1;
            }
        }
    }

    Position DefineEntry(List<Position> _region)
    {
        List<Position> borderRegion = FindBorders(_region);
        int selected = UnityEngine.Random.Range(0, borderRegion.Count - 1);
        int selectedX = borderRegion[selected].posX;
        int selectedY = borderRegion[selected].posY;
        drawMap[selectedX, selectedY] = 2;
        Position entry = new Position(selectedX, selectedY);
        return entry;
    }

    void DefineExit(List<Position> _region, Position _entry)
    {
        List<Position> exits = new List<Position>();
        List<Position> borderRegion = FindBorders(_region);
        int selected, selectedX = 0, selectedY = 0;

        for (int i = 0; i < nbExit; i++)
        {
            bool success = false;
            int tries = 0;
            while (!success)
            {
                selected = UnityEngine.Random.Range(0, borderRegion.Count - 1);
                selectedX = borderRegion[selected].posX;
                selectedY = borderRegion[selected].posY;
                if (drawMap[selectedX, selectedY] == 0 &&    // The tile must be free...
                    EuclidianDistance(selectedX, selectedY, _entry.posX, _entry.posY) > 5 + EuclidianDistance(0, 0, width, height) / 10  // ... be far enough from the entry...
                    && DistanceToExits(exits, selectedX, selectedY) > 5 + EuclidianDistance(0, 0, width, height) / 10)   // ... and any other exit
                {
                    drawMap[selectedX, selectedY] = 3;
                    success = true;
                }
                tries++;
                if (tries > borderRegion.Count * 2)
                    success = true;
            }
            if (tries > borderRegion.Count * 2)
                break;
            exits.Add(new Position(selectedX, selectedY));
        }
    }

    List<Position> FindBorders(List<Position> _region)
    {
        List<Position> borders = new List<Position>();
        foreach (Position tile in _region)
        {
            if (RegionBorder(_region, tile.posX, tile.posY) > 1)
                borders.Add(tile);
        }
        return borders;
    }

    int RegionBorder(List<Position> _region, int x, int y)
    {
        int borders = 0;
        if (!_region.Contains(new Position(x + 1, y)))
            borders++;
        if (!_region.Contains(new Position(x, y + 1)))
            borders++;
        if (!_region.Contains(new Position(x - 1, y)))
            borders++;
        if (!_region.Contains(new Position(x, y - 1)))
            borders++;
        return borders;
    }

    float DistanceToExits(List<Position> _exits, int _selectedX, int _selectedY)
    {
        float distance = EuclidianDistance(0, 0, width, height);
        foreach (Position exit in _exits)
        {
            distance = Mathf.Min(distance, EuclidianDistance(_selectedX, _selectedY, exit.posX, exit.posY));
        }
        return distance;
    }

    float EuclidianDistance(int x1, int y1, int x2, int y2)
    {
        return Mathf.Sqrt(Mathf.Abs(x1 - x2) * Mathf.Abs(x1 - x2) + Mathf.Abs(y1 - y2) * Mathf.Abs(y1 - y2));
    }

    void OnDrawGizmos()
    {
        Vector3 gizmoOffset = new Vector3(width * 2, 0, height * 2);
        if (wallHorizontal != null)     // Draw horizontal walls
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height + 1; y++)
                {
                    if (wallHorizontal[x, y] != 0)
                    {
                        Vector3 from = new Vector3(x, 0, y);
                        Vector3 to = new Vector3(x + 1, 0, y);
                        Gizmos.color = (wallHorizontal[x, y] == 1) ? Color.white : Color.white;
                        Gizmos.DrawLine(from - gizmoOffset, to - gizmoOffset);
                    }
                }
            }
        }

        if (wallVertical != null)       // Draw vertical walls
        {
            for (int x = 0; x < width + 1; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (wallVertical[x, y] != 0)
                    {
                        Vector3 from = new Vector3(x, 0, y);
                        Vector3 to = new Vector3(x, 0, y + 1);
                        Gizmos.color = (wallVertical[x, y] == 1) ? Color.white : Color.white;
                        Gizmos.DrawLine(from - gizmoOffset, to - gizmoOffset);
                    }
                }
            }
        }

        if (drawMap != null)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (drawMap[x, y] == 1)     // Draw blocks
                    {
                        Gizmos.color = Color.white;
                        Vector3 pos = new Vector3(x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(1.05f, 1.05f, 1.05f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }
                    if (drawMap[x, y] > 1)      // Draw entry and exits
                    {
                        Gizmos.color = (drawMap[x, y] == 2) ? Color.blue : Color.red;
                        Vector3 pos = new Vector3(x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(0.5f, 0.5f, 0.5f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }
                }
            }
            for (int x = -width / 6; x < width + width / 6; x++)    // Draw Border
            {
                for (int y = -height / 6; y < height + height / 6; y++)
                {
                    if (!IsInMapRange(x, y))
                    {
                        Gizmos.color = Color.white;
                        Vector3 pos = new Vector3(x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(1.05f, 1.05f, 1.05f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }
                }
            }
        }
    }

    void Make3D()
    {
        GameObject parentWalls = new GameObject("Walls");
        GameObject parentRegions = new GameObject("Idle Regions");
        instantiated.Add(parentWalls);
        instantiated.Add(parentRegions);

        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane); // Instantiate floor
        plane.transform.position = new Vector3(width * scale / 2, 0, height * scale / 2);
        plane.transform.localScale = new Vector3(width * scale / 8, 1, height * scale / 8);
        instantiated.Add(plane);

        List<GameObject> wallType = new List<GameObject>();

        for (int i = 0; i < wallDiversity; i++)
            wallType.Add(wallGenerator.GenerateWall());
        //wallType.Add(wall);

        for (int x = 0; x < width; x++) // Instantiate Horizontal Walls
        {
            for (int y = 0; y < height + 1; y++)
            {
                if (wallHorizontal[x, y] != 0)
                {
                    Vector3 pos = new Vector3(x * scale, 0, y * scale + 0.1f);
                    Quaternion rot = Quaternion.Euler(0, 0, 0);
                    GameObject newWall = Instantiate(wallType[UnityEngine.Random.Range(0, wallType.Count)]);
                    newWall.transform.position = pos;
                    newWall.transform.rotation *= rot;
                    newWall.transform.parent = parentWalls.transform;
                }
            }
        }

        for (int x = 0; x < width + 1; x++) // Instantiate Vertical Walls
        {
            for (int y = 0; y < height; y++)
            {
                if (wallVertical[x, y] != 0)
                {
                    Vector3 pos = new Vector3(x * scale - 0.1f, 0, y * scale);
                    Quaternion rot = Quaternion.Euler(0, 0, -90);
                    GameObject newWall = Instantiate(wallType[UnityEngine.Random.Range(0, wallType.Count)]);
                    newWall.transform.position = pos;
                    newWall.transform.rotation *= rot;
                    newWall.transform.parent = parentWalls.transform;
                }
            }
        }

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (drawMap[x, y] == 1)     // Instantiate blocks
                {
                    Vector3 pos = new Vector3(x * scale + scale / 2, scale / 2, y * scale + scale / 2);
                    GameObject newBlock = Instantiate(block, pos, Quaternion.Euler(0, 0, 0));
                    newBlock.transform.parent = parentRegions.transform;
                }
                if (drawMap[x, y] == 3)      // Instantiate exits
                {
                    Vector3 pos = new Vector3(x * scale + scale / 2, scale / 2, y * scale + scale / 2);
                    instantiated.Add(Instantiate(door, pos, Quaternion.Euler(0, 0, 0)));
                }
                if (drawMap[x, y] == 2)      // Place entry
                {
                    rig.transform.position = new Vector3(x * scale + scale / 2, 0.1f, y * scale + scale / 2);
                }
            }
        }
        foreach (GameObject _wall in wallType)
            Destroy(_wall);
    }
}
