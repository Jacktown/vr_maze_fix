﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instantiation : MonoBehaviour {

    public GameObject flare;
    public GameObject pebble;
    public Text textFlares;
    public Text textPebbles;

    private int nbFlares;
    private int nbPebbles;
    bool flareEnable = false;
    bool pebbleEnable = false;

    List<GameObject> items = new List<GameObject>();

    private void Start()
    {
        nbFlares = 1;
        nbPebbles = 10;
        UpdateText();
    }

    private void UpdateText()
    {
        textFlares.text = nbFlares + "x";
        textPebbles.text = nbPebbles + "x";
    }

    public void InstantiateItem()
    {
        if (flareEnable && nbFlares > 0)
        {
            Vector3 getPos = transform.position;
            items.Add(Instantiate(flare, getPos - new Vector3(0, getPos[1] - 0.75f, 0), Quaternion.identity));
            RemoveFlares();
        }
        else
        {
            if (pebbleEnable && nbPebbles > 0)
            {
                Vector3 getPos = transform.position;
                Quaternion getRot = transform.rotation;
                items.Add(Instantiate(pebble, getPos, getRot * Quaternion.Euler(0, 90, 0)));
                RemovePebbles();
            }
        }
    }

    public void DestroyItems()
    {
        foreach (GameObject toDestroy in items)
            Destroy(toDestroy);
    }

    public void ActivateTool(int choice)
    {
        switch (choice)
        {
            case 0:
                flareEnable = false;
                pebbleEnable = false;
                break;
            case 1:
                flareEnable = true;
                pebbleEnable = false;
                break;
            case 2:
                flareEnable = false;
                pebbleEnable = true;
                break;
            case 3:
                break;
            default:
                break;
        }
    }

    public void AddFlares()
    {
        nbFlares += UnityEngine.Random.Range(0, 3);
        UpdateText();
    }

    public void RemoveFlares()
    {
        nbFlares--;
        UpdateText();
    }

    public void AddPebbles()
    {
        nbPebbles += UnityEngine.Random.Range(5, 10);
        UpdateText();
    }

    public void RemovePebbles()
    {
        nbPebbles--;
        UpdateText();
    }

    public int GetnbFlares()
    {
        return nbFlares;
    }

    public int GetnbPebbles()
    {
        return nbPebbles;
    }

    public void ResetItems()
    {
        nbFlares = 1;
        nbPebbles = 10;
        UpdateText();
    }
}
