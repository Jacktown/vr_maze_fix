﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    public GameObject controller;
    private bool isOpen;
    Instantiation instantiator;
    private GameObject StatSaver;
    private Stats _stats;

    void Start () {
        isOpen = false;
        instantiator = controller.GetComponent<Instantiation>();
        StatSaver = GameObject.Find("StatSaver");
        _stats = StatSaver.GetComponent<Stats>();
    }
		
	public void Open () {
        if (!isOpen)
        {
            isOpen = true;
            _stats.ChestOpened();
            instantiator.AddFlares();
            instantiator.AddPebbles();
        }
    }
}
