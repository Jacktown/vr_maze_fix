﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGeneratation : MonoBehaviour {

    [Header("Standard Settings")]
    [Range(10, 50)]
    public int width=20;
    [Range(10, 50)]
    public int height=20;
    [Range(1, 6)]
    public int nbExit=2;
    [Range(1, 10)]
    public int nbChestMax = 5;
    private int nbChestMin = 2;

    [Header("Advanced Settings")]
    [Range(20, 75)]
    public int horizontalPercentage = 45;
    [Range(20, 95)]
    public int verticalPercentage = 90;
    [Range(40, 95)]
    public int roomPercentage = 75;
    [Range(1, 100)]
    public int wallDiversity = 20;
    public bool proceduralWalls = false;

    [Header("Assets")]
    public GameObject wall;
    public GameObject block;
    public GameObject door;
    public GameObject chest;
    public GameObject rig;
    
    int[,] wallHorizontal;
    int[,] wallVertical;
    int[,] drawMap;
    float scale = 5f;
    int score = 0;

    List<GameObject> instantiated = new List<GameObject>();

    WallGeneration wallGenerator;
    public Screenshots photographer;

    struct Position
    {
        public int posX;
        public int posY;

        public Position(int x, int y)
        {
            posX = x;
            posY = y;
        }
    }

    void Start()
    {
        wallGenerator = gameObject.GetComponent<WallGeneration>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            GenerateMap();
        }
    }

    public void GenerateMap()
    {
        if(instantiated.Count != 0)		// if a maze  already exists, 
        {
            score++;					// increment your score and delete the maze
            DestroyMaze();
        }
        int nbTry = 0;
        List<List<Position>> regions;
        do
        {
            wallHorizontal = new int[width, height + 1];	// Array of walls. 1 if wall, else 0
            wallVertical = new int[width + 1, height];
			drawMap = new int[width, height];				// Element on the map (exit, chest, ...)
            FillMap();										// fill horizontal and vertical wall map randomly depending on % defined
            FixMap();										// join isolated walls to others by randomly creating another one according some conditions
            regions = GetRegions();							// cf function
            regions.Sort((a, b) => a.Count - b.Count);		// sort regions by increasing size
            nbTry++;
        } while (regions[regions.Count - 1].Count < (width * height * (roomPercentage / 100.0f)) && nbTry < 5000);
        List<Position> borderRegion = FindBorders(regions[regions.Count - 1]); //define as border all the tiles in the biggest region that have at least 2 neighbours from another region
        Position entryPos = DefineEntry(borderRegion);  	// randomly define the entry point among the border tiles
		DefineExit(borderRegion, entryPos);					// randomly define the exit points among the border tiles, but from a distance a far enough from the entry
        DefineChest(regions[regions.Count - 1], entryPos);	// same than DefineExit with chests
		DrawBlocks(regions);								// defines ALL the tiles that are not in the active region (not reachable tiles) as blocks 
        //Debug.Log("Number of regions: " + regions.Count);
        //Debug.Log("Size of biggest region: " + regions[regions.Count - 1].Count);
        //Debug.Log("Size of smallest region: " + regions[0].Count);
        //Debug.Log("Room treshold: " + width * height * (roomPercentage / 100.0f));
        //Debug.Log("Number of tries: " + nbTry);
        //Debug.Log("Score: " + score);
        Make3D();											// generate the 3D labyrinthe using the vertical, horizontal and draw map
		photographer.TakePicture(width, height);
    }

    void FillMap()
    {
        for (int y = 0; y < wallHorizontal.GetLength(1); y++) 			// y = columns
        {
            for (int x = 0; x < wallHorizontal.GetLength(0); x++)		// x = raws
            {
                if (y == 0 || y == wallHorizontal.GetLength(1) - 1)		// if maze border ==> wall compulsory
                {
                    wallHorizontal[x, y] = 1;
                }
                else  													// else create randomly a wall according to horizontal %
                {
                    wallHorizontal[x, y] = (UnityEngine.Random.Range(0, 100) < horizontalPercentage) ? 1 : 0;
                }
            }
        }

        for (int y = 0; y < wallVertical.GetLength(1); y++)
        {
            for (int x = 0; x < wallVertical.GetLength(0); x++) 
            {
				if (x == 0 || x == wallVertical.GetLength(0) - 1) // if maze border ==> wall compulsory
                {
                    wallVertical[x, y] = 1;
                }
                else {
                    switch (wallHorizontal[x - 1, y] + wallHorizontal[x - 1, y + 1] + wallHorizontal[x, y] + wallHorizontal[x, y + 1])
                    {											// set a wall according to the surrounding walls 
                        case 0:
                            wallVertical[x, y] = 1;				// if no horizontal wall arround ==> there will be a vertical one
                            break;
						case 1:									// if X walls ==> chances = (vertical %) / 2^(X-1)
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage) ? 1 : 0;
                            break;
                        case 2:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 2) ? 1 : 0;
                            break;
                        case 3:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 4) ? 1 : 0;
                            break;
                        case 4:
                            wallVertical[x, y] = (UnityEngine.Random.Range(0, 100) < verticalPercentage / 8) ? 1 : 0;
                            break;
                        default:
                            wallVertical[x, y] = wallVertical[x, y];
                            break;
                    }
                }
            }
        }
    }

    List<List<Position>> GetRegions()
    {
		/* Look each tile in the maze. If a tiles is not flagged, calls GetRegion()
		 * GetRegion : return a list of position of each reachable tiles from the observed tile and flag all that tiles
		 * GetRegions will return the list of all the position lists obtained
		 */
        List<List<Position>> regions = new List<List<Position>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0)
                {
                    List<Position> newRegion = GetRegion(x, y);
                    regions.Add(newRegion);

                    foreach (Position tile in newRegion)
                    {
                        mapFlags[tile.posX, tile.posY] = 1;
                    }
                }
            }
        }
        return regions;
    }

    List<Position> GetRegion(int startX, int startY)
    {
        List<Position> region = new List<Position>();
        int[,] mapFlags = new int[width, height];

        Queue<Position> toVisit = new Queue<Position>();
        toVisit.Enqueue(new Position(startX, startY));
        mapFlags[startX, startY] = 1;

        while (toVisit.Count > 0)
        {
            Position tile = toVisit.Dequeue();
            region.Add(tile);

            if (IsInMapRange(tile.posX, tile.posY - 1) && mapFlags[tile.posX, tile.posY - 1] == 0 && wallHorizontal[tile.posX, tile.posY] == 0)
            {
                mapFlags[tile.posX, tile.posY - 1] = 1;
                toVisit.Enqueue(new Position(tile.posX, tile.posY - 1));
            }
            if (IsInMapRange(tile.posX - 1, tile.posY) && mapFlags[tile.posX - 1, tile.posY] == 0 && wallVertical[tile.posX, tile.posY] == 0)
            {
                mapFlags[tile.posX - 1, tile.posY] = 1;
                toVisit.Enqueue(new Position(tile.posX - 1, tile.posY));
            }
            if (IsInMapRange(tile.posX, tile.posY + 1) && mapFlags[tile.posX, tile.posY + 1] == 0 && wallHorizontal[tile.posX, tile.posY + 1] == 0)
            {
                mapFlags[tile.posX, tile.posY + 1] = 1;
                toVisit.Enqueue(new Position(tile.posX, tile.posY + 1));
            }
            if (IsInMapRange(tile.posX + 1, tile.posY) && mapFlags[tile.posX + 1, tile.posY] == 0 && wallVertical[tile.posX + 1, tile.posY] == 0)
            {
                mapFlags[tile.posX + 1, tile.posY] = 1;
                toVisit.Enqueue(new Position(tile.posX + 1, tile.posY));
            }
        }
        return region;
    }

    void FixMap()
    {
        int isolatedCountH = 0;
        int isolatedCountV = 0;
        for (int y = 0; y < wallHorizontal.GetLength(1); y++) 				// column
        {
            for (int x = 0; x < wallHorizontal.GetLength(0); x++) 			// raws
            {
				if (IsIsolated(x, y, false) && wallHorizontal[x, y] == 1) 	// if an horizontal wall (not a border) is isolated
                {
                    isolatedCountH++;
                    LinkToNeighbour(x, y,false);							// create randomly-ish a wall to link wall to other walls.	
                }
            }
        }

        for (int y = 0; y < wallVertical.GetLength(1); y++)
        {
            for (int x = 0; x < wallVertical.GetLength(0); x++)
            {
                if (IsIsolated(x, y, true) && wallVertical[x, y] == 1)
                {
                    isolatedCountV++;
                    LinkToNeighbour(x, y,true);
                }
            }
        }
        //Debug.Log("Isolated Horizontal walls: " + isolatedCountH + " ; Isolated Vertical walls: " + isolatedCountV);
        //Debug.Log("Horizontal walls: " + CountHorizontalWalls() + " ; Vertical walls: " + CountVerticalWalls());
    }

    int CountHorizontalWalls()
    {
        int horiCount = 0;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height + 1; y++)
            {
                if (wallHorizontal[x, y] != 0)
                {
                    horiCount++;
                }
            }
        }
        return horiCount;
    }

    int CountVerticalWalls()
    {
        int vertCount = 0;
        for (int x = 0; x < width + 1; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (wallVertical[x, y] != 0)
                {
                    vertCount++;
                }
            }
        }
        return vertCount;
    }

    bool IsIsolated(int x, int y, bool vertical)
    {
		/* if the wall is not part of the border and
		 * and if the wall does not touch any other wall, 
		 * then it is isolated
		 */
        bool ret = false;
        if (vertical)
        {
            if (x != 0 && x != wallVertical.GetLength(0) - 1 && y != 0 && y != wallVertical.GetLength(1) - 1)
            {
                if (wallHorizontal[x, y] == 0 && wallHorizontal[x, y + 1] == 0 && wallHorizontal[x - 1, y] == 0 && wallHorizontal[x - 1, y + 1] == 0 && wallVertical[x, y - 1] == 0 && wallVertical[x, y + 1] == 0)
                    ret = true;
            }
        }
        else
        {
            if (y != 0 && y != wallHorizontal.GetLength(1) - 1 && x != 0 && x != wallHorizontal.GetLength(0) - 1)
            {
                if (wallVertical[x, y] == 0 && wallVertical[x, y - 1] == 0 && wallVertical[x + 1, y] == 0 && wallVertical[x + 1, y - 1] == 0 && wallHorizontal[x - 1, y] == 0 && wallHorizontal[x + 1, y] == 0)
                    ret = true;
            }
        }
        return ret;
    }

    void LinkToNeighbour(int x, int y, bool vertical)
    {
        if (CountHorizontalWalls()>CountVerticalWalls())    //add vertical walls
        {
            if (vertical)
            {
                if (UnityEngine.Random.Range(0, 100)<50)
                {
                    wallVertical[x, y + 1] = 2;
                }
                else
                {
                    wallVertical[x, y - 1] = 2;
                }
            }
            else
            {
                bool solved = false;
                while (!solved)
                {
                    switch(UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                            if (wallHorizontal[x - 1, y - 1] == 1 || wallHorizontal[x, y - 1] == 1 || IsReachableVertical(x, y - 2))
                            {
                                solved = true;
                                wallVertical[x, y - 1] = 2;
                            }
                            break;
                        case 1:
                            if (wallHorizontal[x - 1, y + 1] == 1 || wallHorizontal[x, y + 1] == 1 || IsReachableVertical(x, y + 1))
                            {
                                solved = true;
                                wallVertical[x, y] = 2;
                            }
                            break;
                        case 2:
                            if (wallHorizontal[x + 1, y + 1] == 1 || wallHorizontal[x, y + 1] == 1 || IsReachableVertical(x + 1, y + 1))
                            {
                                solved = true;
                                wallVertical[x + 1, y] = 2;
                            }
                            break;
                        case 3:
                            if (wallHorizontal[x + 1, y - 1] == 1 || wallHorizontal[x, y - 1] == 1 || IsReachableVertical(x + 1, y - 2))
                            {
                                solved = true;
                                wallVertical[x + 1, y - 1] = 2;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        else   //add horizontal walls
        {
            if (vertical)
            {
                bool solved = false;
                while (!solved)
                {
                    switch (UnityEngine.Random.Range(0, 4))
                    {
                        case 0:
                            if (wallVertical[x - 1, y - 1] == 1 || wallVertical[x -1, y] == 1 || IsReachableHorizontal(x - 2, y))
                            {
                                solved = true;
                                wallHorizontal[x - 1, y] = 2;
                            }
                            break;
                        case 1:
                            if (wallVertical[x - 1, y + 1] == 1 || wallVertical[x -1 , y] == 1 || IsReachableHorizontal(x - 2, y + 1))
                            {
                                solved = true;
                                wallHorizontal[x - 1, y + 1] = 2;
                            }
                            break;
                        case 2:
                            if (wallVertical[x + 1, y + 1] == 1 || wallVertical[x + 1, y] == 1 || IsReachableHorizontal(x + 1, y + 1))
                            {
                                solved = true;
                                wallHorizontal[x, y + 1] = 2;
                            }
                            break;
                        case 3:
                            if (wallVertical[x + 1, y - 1] == 1 || wallVertical[x + 1, y] == 1 || IsReachableHorizontal(x + 1, y))
                            {
                                solved = true;
                                wallHorizontal[x, y] = 2;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                if (UnityEngine.Random.Range(0, 100) < 50) 
                {
                    wallHorizontal[x+1, y] = 2;
                }
                else
                {
                    wallHorizontal[x-1, y] = 2;
                }
            }
        }
    }

    bool IsReachableVertical(int x, int y) // test if there is a vert. wall in x, y
    {
        bool ret = false;
        if (y < wallVertical.GetLength(1) && y >= 0)
        {
            if (wallVertical[x, y] == 1)
                ret = true;
        }
        return ret;
    }

	bool IsReachableHorizontal(int x, int y) // test if there is an hor. wall in x, y
    {
        bool ret = false;
        if (x < wallHorizontal.GetLength(0) && x >= 0)
        {
            if (wallHorizontal[x, y] == 1)
                ret = true;
        }
        return ret;
    }

    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    void DrawBlocks(List<List<Position>> _regions)
    {
        for (int i = 0; i < _regions.Count - 1; i++)
        {
            List<Position> newRegion = _regions[i];
            foreach (Position tile in newRegion)
            {
                drawMap[tile.posX, tile.posY] = 1;
            }
        }
    }

    Position DefineEntry(List<Position> _borderRegion)
    {
        int selected = UnityEngine.Random.Range(0, _borderRegion.Count);
        int selectedX = _borderRegion[selected].posX;
        int selectedY = _borderRegion[selected].posY;
        drawMap[selectedX, selectedY] = 2;
        Position entry = new Position(selectedX, selectedY);
        return entry;
    }

    void DefineExit(List<Position> _borderRegion, Position _entry)
    {
        List<Position> exits = new List<Position>();
        int selected, selectedX = 0, selectedY = 0;
        
        for (int i = 0; i < nbExit; i++)
        {
            bool success = false;
            int tries = 0;
            while (!success)
            {
                selected = UnityEngine.Random.Range(0, _borderRegion.Count);
                selectedX = _borderRegion[selected].posX;
                selectedY = _borderRegion[selected].posY;
                if (drawMap[selectedX, selectedY] == 0 &&    // The tile must be free...
                    EuclidianDistance(selectedX, selectedY, _entry.posX, _entry.posY) > 5 + EuclidianDistance(0, 0, width, height) / 10  // ... be far enough from the entry...
                    && DistanceToExits(exits, selectedX, selectedY) > 5 + EuclidianDistance(0, 0, width, height) / 10)   // ... and any other exit
                {
                    drawMap[selectedX, selectedY] = 3;
                    success = true;
                }
                tries++;
                if (tries > _borderRegion.Count*2)
                    success = true;
            }
            if (tries > _borderRegion.Count * 2)
                break;
            exits.Add(new Position(selectedX, selectedY));
        }
    }

    void DefineChest(List<Position> _region, Position _entry)
    {
        List<Position> chests = new List<Position>();
        int selected, selectedX, selectedY;
        int nbChest = UnityEngine.Random.Range(nbChestMin, nbChestMax + 1);
        bool success = false;
        int nbSuccess = 0;
        int tries = 0;

        while (!success)
        {
            selected = UnityEngine.Random.Range(0, _region.Count);
            selectedX = _region[selected].posX;
            selectedY = _region[selected].posY;
            if (drawMap[selectedX, selectedY] == 0 &&    // The tile must be free...
                EuclidianDistance(selectedX, selectedY, _entry.posX, _entry.posY) > 4 + EuclidianDistance(0, 0, width, height) / 10  // ... be far enough from the entry...
                && DistanceToExits(chests, selectedX, selectedY) > 4 + EuclidianDistance(0, 0, width, height) / 12)   // ... and any other exit
            {
                drawMap[selectedX, selectedY] = 4;
                nbSuccess++;
                chests.Add(new Position(selectedX, selectedY));
            }
            tries++;
            if(tries > _region.Count * 2 || nbSuccess == nbChest)
            {
                success = true;
            }
        }

    }

    List<Position> FindBorders(List<Position> _region)
    {
        List<Position> borders = new List<Position>();
        foreach (Position tile in _region)
        {
            if (RegionBorder(_region, tile.posX, tile.posY) > 1)
                borders.Add(tile);
        }
        return borders;
    }

    int RegionBorder(List<Position> _region, int x, int y) // count the number of tiles that are neighbours of the observed one, but not in the same region.
    {
        int borders = 0;
        if (!_region.Contains(new Position(x + 1, y)))
            borders++;
        if (!_region.Contains(new Position(x, y + 1)))
            borders++;
        if (!_region.Contains(new Position(x - 1, y)))
            borders++;
        if (!_region.Contains(new Position(x, y - 1)))
            borders++;
        return borders;
    }

    float DistanceToExits(List<Position> _exits, int _selectedX, int _selectedY)
    {
        float distance = EuclidianDistance(0, 0, width, height);
        foreach(Position exit in _exits)
        {
            distance = Mathf.Min(distance, EuclidianDistance(_selectedX, _selectedY, exit.posX, exit.posY));
        }
        return distance;
    }

    float DistanceToChests(List<Position> _chests, int _selectedX, int _selectedY)
    {
        float distance = EuclidianDistance(0, 0, width, height);
        foreach (Position exit in _chests)
        {
            distance = Mathf.Min(distance, EuclidianDistance(_selectedX, _selectedY, exit.posX, exit.posY));
        }
        return distance;
    }

    float EuclidianDistance(int x1, int y1, int x2, int y2)
    {
        return Mathf.Sqrt(Mathf.Abs(x1 - x2)* Mathf.Abs(x1 - x2) + Mathf.Abs(y1 - y2) * Mathf.Abs(y1 - y2));
    }

    Quaternion OrientateChest(int posX, int posY)
    {
        Quaternion result = Quaternion.Euler(0, 0, 0);
        bool success = false;
        while (!success)
        {
            int selected = UnityEngine.Random.Range(0, 4);
            switch (selected)
            {
                case 0:
                    if (wallHorizontal[posX,posY+1]==0)
                    {
                        result = Quaternion.Euler(0, 0, 0);
                        success = true;
                    }
                    break;
                case 2:
                    if (wallHorizontal[posX, posY] == 0)
                    {
                        result = Quaternion.Euler(0, 180, 0);
                        success = true;
                    }
                    break;
                case 1:
                    if (wallVertical[posX, posY] == 0)
                    {
                        result = Quaternion.Euler(0, -90, 0);
                        success = true;
                    }
                    break;
                case 3:
                    if (wallVertical[posX + 1, posY] == 0)
                    {
                        result = Quaternion.Euler(0, 90, 0);
                        success = true;
                    }
                    break;
                default:
                    success = true;
                    break;
            }
        }
        return result;
    }

    void OnDrawGizmos()
    {
        Vector3 gizmoOffset = new Vector3(30 + width/2, 0, 30 + height/2);
        if (wallHorizontal != null)     // Draw horizontal walls
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height+1; y++)
                {
                    if(wallHorizontal[x, y] !=0)
                    {
                        Vector3 from = new Vector3(x,0,y);
                        Vector3 to = new Vector3(x+1, 0, y);
                        Gizmos.color =(wallHorizontal[x, y] == 1) ? Color.white : Color.white;
                        Gizmos.DrawLine(from-gizmoOffset, to-gizmoOffset);
                    }
                }
            }
        }

        if (wallVertical != null)       // Draw vertical walls
        {
            for (int x = 0; x < width+1; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (wallVertical[x, y] != 0)
                    {
                        Vector3 from = new Vector3(x, 0, y);
                        Vector3 to = new Vector3(x, 0, y+1);
                        Gizmos.color = (wallVertical[x, y] == 1) ? Color.white : Color.white;
                        Gizmos.DrawLine(from - gizmoOffset, to - gizmoOffset);
                    }
                }
            }
        }

        if (drawMap != null)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (drawMap[x, y] == 1)     // Draw blocks
                    {
                        Gizmos.color = Color.white;
                        Vector3 pos = new Vector3( x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(1.05f, 1.05f, 1.05f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }
                    if (drawMap[x, y] > 1)      // Draw entry and exits
                    {
                        Gizmos.color = (drawMap[x, y] == 2) ? Color.blue : Color.red;
                        Vector3 pos = new Vector3(x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(0.5f, 0.5f, 0.5f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }
                    if (drawMap[x, y] == 4)      // Draw chests
                    {
                        Gizmos.color = Color.yellow;
                        Vector3 pos = new Vector3(x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(0.5f, 0.5f, 0.5f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }

                }
            }
            for (int x = -width / 6; x < width + width / 6; x++)    // Draw Border
            {
                for (int y = -height / 6; y < height + height / 6; y++)
                {
                    if (!IsInMapRange(x, y))
                    {
                        Gizmos.color = Color.white;
                        Vector3 pos = new Vector3(x + .5f, 0, y + .5f);
                        Vector3 size = new Vector3(1.05f, 1.05f, 1.05f);
                        Gizmos.DrawCube(pos - gizmoOffset, size);
                    }
                }
            }
        }

    }

    void Make3D()
    {
		// create 4 empty game objects
        GameObject parentWalls = new GameObject("Walls");
        GameObject parentRegions = new GameObject("Idle Regions");
        GameObject parentExits = new GameObject("Exits");
        GameObject parentChests = new GameObject("Chests");

        instantiated.Add(parentWalls);
        instantiated.Add(parentRegions);
        instantiated.Add(parentExits);
        instantiated.Add(parentChests);

		// Instantiate floor
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube); 
        cube.transform.position = new Vector3(width * scale / 2, 0, height * scale / 2);
        cube.transform.localScale = new Vector3(width * scale, 0.25f, height * scale);
        instantiated.Add(cube);

		// walls
        List<GameObject> wallType = new List<GameObject>();
        float correction = 0f;
        float procShift = 0f;

        if (proceduralWalls)
        {
            procShift = 0.2f;
            for (int i = 0; i < wallDiversity; i++)
                wallType.Add(wallGenerator.GenerateWall());
        }
        else
        {
            correction = 0.2f;
            wallType.Add(wall); // a simple prefab
        }

        for (int x = 0; x < width; x++) // Instantiate Horizontal Walls
        {
            for (int y = 0; y < height + 1; y++)
            {
                if (wallHorizontal[x, y] != 0)
				{	// take a random wall model in the wall type list and position it
                    Vector3 pos = new Vector3(x * scale, 0, y * scale + procShift);
                    Quaternion rot = Quaternion.Euler(0, 0, 0);
                    GameObject newWall = Instantiate(wallType[UnityEngine.Random.Range(0, wallType.Count)]); 
                    newWall.transform.position = pos;
                    newWall.transform.rotation *= rot;
                    if(!proceduralWalls)
                        newWall.transform.localScale = new Vector3(1.04f, 1, 1);
                    newWall.transform.parent = parentWalls.transform;
                }
            }
        }

        for (int x = 0; x < width + 1; x++) // Instantiate Vertical Walls
        {
            for (int y = 0; y < height; y++)
            {
                if (wallVertical[x, y] != 0)
                {
                    Vector3 pos = new Vector3(x * scale + correction - procShift, 0, y * scale + correction);
                    Quaternion rot = Quaternion.Euler(0, 0, -90);
                    GameObject newWall = Instantiate(wallType[UnityEngine.Random.Range(0, wallType.Count)]);
                    newWall.transform.position = pos;
                    newWall.transform.rotation *= rot;
                    newWall.transform.parent = parentWalls.transform;
                }
            }
        }

		// Place every other element on the map (use the drawmap)
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector3 pos = new Vector3(x * scale + scale / 2, scale / 2, y * scale + scale / 2);
                switch (drawMap[x, y]){
                    case 1:     // Instantiate blocks
                        GameObject newBlock = Instantiate(block, pos, Quaternion.Euler(0, 0, 0));
                        newBlock.transform.parent = parentRegions.transform;
                        break;
                    case 2:     // Place entry
                        rig.transform.position = new Vector3(x * scale + scale / 2, 0.1f, y * scale + scale / 2);
                        break;
                    case 3:     // Instantiate exits
                        GameObject newExit =Instantiate(door, pos, Quaternion.Euler(0, 0, 0));
                        newExit.transform.parent = parentExits.transform;
                        break;
                    case 4: //instantiate chest
                        GameObject newChest = Instantiate(chest, pos - new Vector3(0, 2.4f, 0), OrientateChest(x,y));
                        newChest.transform.parent = parentChests.transform;
                        break;
                }
            }
        }
        if (proceduralWalls)
        {
            foreach (GameObject _wall in wallType)
                Destroy(_wall);
        }
    }

    public void SetWidth(float newWidth)
    {
        width = (int)newWidth;
    }

    public void SetHeight(float newHeight)
    {
        height = (int)newHeight;
    }

    public void SetExit(float newExit)
    {
        nbExit = (int)newExit;
    }

    public void SetProcedural(bool isProcedural)
    {
        proceduralWalls = isProcedural;
    }

    public void ResetScore()
    {
        score = 0;
    }

	public int GetScore(){
		return score;
	}

    public void DestroyMaze()
    {
        foreach (GameObject toDestroy in instantiated)
            Destroy(toDestroy);
    }
}
