﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCreation : MonoBehaviour {
    WallGeneration wallGenerator;
    public int wallDiversity;
    public int wallNumber;

    void Start () {
        //Debug.Log("Start [Get wallGenerator]" + Time.realtimeSinceStartup);
        wallGenerator = gameObject.GetComponent<WallGeneration>();
        //Debug.Log("End [Get wallGenerator]" + Time.realtimeSinceStartup);
    }

    void Update () {

        if (Input.GetMouseButtonDown(1))
        {
            //Debug.Log("Start [WallCreation]" + Time.realtimeSinceStartup);
            List<GameObject> wallType = new List<GameObject>();
            //Debug.Log("Start [Create wallType]" + Time.realtimeSinceStartup);
            for (int i = 0; i < wallDiversity; i++)
                wallType.Add(wallGenerator.GenerateWall());
            //Debug.Log("End [Create wallType]" + Time.realtimeSinceStartup);

            //Debug.Log("Start [Instantiate Walls]" + Time.realtimeSinceStartup);
            for (int i = 0; i < wallNumber; i++)
            {
                /*GameObject newWall =*/ Instantiate(wallType[UnityEngine.Random.Range(0, wallType.Count)]);
            }
            //Debug.Log("End [Instantiate Walls]" + Time.realtimeSinceStartup);
            foreach (GameObject _wall in wallType)
                Destroy(_wall);
            //Debug.Log("End [WallCreation]" + Time.realtimeSinceStartup);
        }
    }
}
