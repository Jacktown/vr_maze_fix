﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour {

	public Text stat;

	float timer;
	float beginTimer;
	float endTimer;

	private int chestOpened;

	private GameObject mapGen;
	MapGeneratation mapGenSc;

	// Use this for initialization
	void Start () {
		ClearStats();
		timer = 0;
		mapGen = GameObject.Find("MapGenerator");
		mapGenSc = mapGen.GetComponent<MapGeneratation>();
	}
	
	// Update is called once per frame
	private void Update () {
		timer += Time.deltaTime;		
	}

	public void SetStartTime(){
		beginTimer = timer;
	}

	public void SetEndTime(){
		endTimer = timer;
	}


	private string ConvertFloatToTime(float delta){
		int min, sec;
		min = (int)delta / 60;
		sec = (int)delta % 60;
		return min + "m, " + sec + "s";
	}

	public void ChestOpened()
	{
		chestOpened++;
	}

	public void UpdateStat(){
		string strTime = ConvertFloatToTime (endTimer - beginTimer);
		string lvl = "Level: " + mapGenSc.GetScore () + "\n";
		string time = "Time: " + strTime + "\n"; 
		string chestDiscovered = "Chest: " + chestOpened + "\n"; 
		
		string strVar = lvl + time + chestDiscovered;
		stat.text += strVar;
	}

	public void ClearStats()
	{
		stat.text = "Stats :\n";
		chestOpened = 0;
		SetStartTime();
	}
}
