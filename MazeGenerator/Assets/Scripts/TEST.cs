﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TEST : MonoBehaviour {

    public GameObject camMap;
    public Image imageUI;

    private void OnEnable()
    {
        camMap.SetActive(true);
        StartCoroutine(ExecuteAfterTime(1));
        
        Debug.Log("Picture taken");
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        StartCoroutine(TakePicture());
    }

    IEnumerator TakePicture()
    {
        yield return new WaitForEndOfFrame();

        Texture2D tex = new Texture2D(530, 500, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(10, 10, 520, 490), 5, 5, false);
        tex.Apply();
        Sprite levelMap = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0), 100.0f);
        imageUI.sprite = levelMap;
        camMap.SetActive(false);
    }
}
