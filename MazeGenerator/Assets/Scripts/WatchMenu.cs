﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WatchMenu : MonoBehaviour {

    Quaternion rot;
    Canvas watchCanvas;
	// Use this for initialization
	void Start () {
        watchCanvas = GetComponentInChildren<Canvas>();
    }
	
	// Update is called once per frame
	void Update () {
        rot = transform.rotation;
        //Debug.Log(rot.eulerAngles[2]);
        if (rot.eulerAngles[2] > 45 && rot.eulerAngles[2] < 135)
        {
            watchCanvas.enabled = true;
        }
        else
        {
            watchCanvas.enabled = false;
        }
    }
}
