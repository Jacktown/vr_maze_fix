﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshCombiner : MonoBehaviour
{
    public Material wallMaterial;
    void Start()
    {
        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;
        Vector3 oldScale = transform.localScale;

        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;

        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        Mesh finalMesh = new Mesh();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];

        int i = 0;
        for (i = 0; i < meshFilters.Length; i++)
        {
            if (meshFilters[i].transform == transform)
                continue;
            combine[i].subMeshIndex = 0;
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
        }
        finalMesh.CombineMeshes(combine);
        GetComponent<MeshFilter>().sharedMesh = finalMesh;

        transform.rotation = oldRot;
        transform.position = oldPos;
        transform.localScale = oldScale * 20;

        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        Material mat = GetComponent<Renderer>().material;
        mat = wallMaterial;
        Destroy(this);
    }
}