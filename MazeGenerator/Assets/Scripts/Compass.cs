﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour {

    Text compassText;
    Canvas compassCanvas;

    Vector3 pos;
    Quaternion rot;

    float oldTime;
    string strRot;
    bool compassActive = false;
    
    void Start () {
        compassText = GetComponentInChildren<Text>();
        compassCanvas = GetComponentInChildren<Canvas>();
        oldTime = 0;
        rot = transform.rotation;
    }
	
	void Update () {
        if (compassActive)
        {
            compassCanvas.enabled = true;
            pos = transform.position;
            if (Time.fixedTime - oldTime > 0.5f)
            {
                rot = transform.rotation;
                strRot = StringRotation((int)rot.eulerAngles[1]);
                oldTime = Time.fixedTime;
            }
            compassText.text = "X: " + (int)pos[0]/5 + "\nY: " + (int)pos[2]/5 + "\nDir: " + strRot;
        }
        else
        {
            compassCanvas.enabled = false;
        }
    }

    public void ActivateCompass(bool active)
    {
        compassActive = active;
    }

    string StringRotation(int _rot)
    {
        string text = "";
        if (_rot <= 22 || _rot > 338) { text = "N"; }
        else
        {
            if (_rot <= 67 && _rot > 22) { text = "N/E"; }
            else
            {
                if (_rot <= 112 && _rot > 67) { text = "E"; }
                else
                {
                    if (_rot <= 157 && _rot > 112) { text = "S/E"; }
                    else
                    {
                        if (_rot <= 202 && _rot > 157) { text = "S"; }
                        else
                        {
                            if (_rot <= 247 && _rot > 202) { text = "S/W"; }
                            else
                            {
                                if (_rot <= 292 && _rot > 247) { text = "W"; }
                                else
                                {
                                    if (_rot <= 337 && _rot > 292) { text = "N/W"; }
                                }
                            }
                        }
                    }
                }
            }
        } 
                                        
        return text;
    }
}
